package com.example.myapplication;

import junit.framework.TestCase;
import org.junit.Test;


public class MainActivityTest extends TestCase {

    @Test
    public void testAddEdgeValidEdgeValuesReturnsCorrectPath() {
        int vertices = 2;
        MainActivity.Graph graph = new MainActivity.Graph(vertices);
        graph.addEdge(0,1);
        assertNotNull(graph);
    }

    public void testAddEdgeInvalidEdgeValuesThrowsIndexOutOfBoundsException() {
        int vertices = 2;
        MainActivity.Graph graph = new MainActivity.Graph(vertices);
        try {
            graph.addEdge(-3,-1);
        } catch (ArrayIndexOutOfBoundsException e) {
            return;
        }
    }

    public void testAddEdgeLessVerticesThrowsIndexOutOfBoundsException() {
        int vertices = 2;
        MainActivity.Graph graph = new MainActivity.Graph(vertices);
        try {
            graph.addEdge(3,4);
        } catch (ArrayIndexOutOfBoundsException e) {
            return;
        }
    }

    @Test
    public void testGetShortestPathValidGraphReturnsCorrectPath() {
        int vertices = 7;
        MainActivity.Graph graph = new MainActivity.Graph(vertices);
        graph.addEdge(0, 1);graph.addEdge(1, 2);graph.addEdge(2, 3);
        graph.addEdge(2, 4);graph.addEdge(2, 5);graph.addEdge(5,6);

        String shortestPath = graph.getShortestPath(0,6);

        assertEquals("0 1 2 5 6 ",shortestPath);
    }

    @Test
    public void testGetShortestPathSourceEqualToDestinationReturnsSource() {
        int vertices = 1;
        MainActivity.Graph graph = new MainActivity.Graph(vertices);
        String shortestPath = graph.getShortestPath(1,1);
        assertEquals("1 ",shortestPath);
    }

    @Test
    public void testGetShortestPathInvalidSourceThrowsParameterException() {
        int vertices = 7;
        MainActivity.Graph graph = new MainActivity.Graph(vertices);
        graph.addEdge(0, 1);graph.addEdge(1, 2);graph.addEdge(2, 3);
        graph.addEdge(2, 4);graph.addEdge(2, 5);graph.addEdge(5,6);
        try {
            graph.getShortestPath(-1,6);
        } catch (Exception e) {
            return;
        }
    }

    @Test
    public void testGetShortestPathInvalidDestinationThrowsParameterException() {
        int vertices = 7;
        MainActivity.Graph graph = new MainActivity.Graph(vertices);
        graph.addEdge(0, 1);graph.addEdge(1, 2);graph.addEdge(2, 3);
        graph.addEdge(2, 4);graph.addEdge(2, 5);graph.addEdge(5,6);
        try {
            graph.getShortestPath(1,-6);
        } catch (Exception e) {
            return;
        }
    }

    @Test
    public void testStringToArrayValidStringReturnsCorrectArray() {
        String shortestPath = "0 1 2 5 6 ";

        int[] testArray = new int[5];
        testArray[0] = 0; testArray[1] = 1; testArray[2] = 2;
        testArray[3] = 5; testArray[4] = 6;

        int[] array;
        array = MainActivity.stringToArray(shortestPath);

        for (int i = 0; i < 5; i++)
            assertEquals(testArray[i],array[i]);
    }

    @Test
    public void testStringToArrayEmptyStringThrowsException() {
        String shortestPath = "";
        try{
            MainActivity.stringToArray(shortestPath);
        } catch (Exception e) {
            return;
        }
    }

    @Test
    public void testStringToArrayInvalidStringThrowsException() {
        String shortestPath = "- - -";
        try{
            MainActivity.stringToArray(shortestPath);
        } catch (Exception e) {
            return;
        }
    }

    @Test
    public void testGroupPathsValidIntArrayReturnsCorrectStringArray() {
        int[] testIntegerArray = new int[5];
        testIntegerArray[0] = 0; testIntegerArray[1] = 1; testIntegerArray[2] = 2;
        testIntegerArray[3] = 5; testIntegerArray[4] = 6;

        String[] testStringArray = new String[testIntegerArray.length-1];
        testStringArray[0] = "0,1"; testStringArray[1] = "1,2";
        testStringArray[2] = "2,5"; testStringArray[3] = "5,6";

        String[] stringArray;
        stringArray = MainActivity.groupPaths(testIntegerArray);

        for (int i = 0; i < 4; i++)
            assertEquals(testStringArray[i],stringArray[i]);
    }

    @Test
    public void testGroupPathsOneDataItemArrayThrowsIndexOutOfBoundsException() {
        int[] testIntegerArray = new int[1];
        testIntegerArray[0] = 0;
        try {
            MainActivity.groupPaths(testIntegerArray);
        } catch (IndexOutOfBoundsException e) {
            return;
        }
    }

    @Test
    public void testGroupPathsEmptyArrayThrowsNegativeArraySizeException() {
        int[] testIntegerArray = new int[0];
        try {
            MainActivity.groupPaths(testIntegerArray);
        } catch (NegativeArraySizeException e) {
            return;
        }
    }
}